function grayToRGB(grayImageSource,colorImageSource, methodNumber, limitDifference)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%UFRGS UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL                          % 
%INF - Instituto de Informática UFRGS                                     %
%                                                                         %
%INF01046 - Fundamentals of Image Processing                              %
%CREATED BY GUILHERME CATTANI AND ARTHUR MEDEIROS                         %
%as an assignment part of Prof. Manuel Menezes de Oliveira Neto lectures  %
%                                                                         %
%Reference Paper: Transferring Color to Greyscale Images                  %
%Authors:Tomihisa Welsh, Michael Ashikhmin, Klaus Mueller                 %
%                                                                         %
%https://classes.soe.ucsc.edu/cmps290b/Fall05/readings/colorize-sig02.pdf %
%                                                                         %
%instructions                                                             %                                        
%methodNumber => 1 region scanning low contrast (BETA)   (Not so OK-ish)  %
%                2 region scanning high contrast (BETA)  (OK-ish)         %
%                3 normalization normal sampling         (fast)           %
%                4 normalization reduced sampling        (fastest)        %   
%limitDifference => lower or equal to 0 = OFF                             %
%                   any other = ON only works in region scanning          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

warning off;
disp ('Initializing...');
grayImage=imread(grayImageSource);
[grayX, grayY, grayZ]=size(grayImage);

colorImage=imread(colorImageSource);
[colorX, colorY, colorZ]=size(colorImage);

%Error messages and handling
if methodNumber > 4 || methodNumber <1
    disp ('A method with this number doesnt exist (yet) select from 1 to 4');
    return
end
    
if grayZ~=1
    disp ('grayImage should have only one channel, fixing...');
    grayImage=rgb2gray(grayImage);
end

if colorZ~=3
    disp ('colorImage need to have 3 channels only, exiting');
    return
else   
    grayImage = cat(3,grayImage(:,:,1),grayImage(:,:,1),grayImage(:,:,1));
    
%RGB to L*a*b conversion    
disp ('Converting to LAB...');     
    colorSpace=rgb2lab(colorImage);
    graySpace= rgb2lab(grayImage);

%Different Methods Handling (region method at line 129)
if(methodNumber > 2) %Normalizing method
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    if methodNumber == 3
    disp('Normalizing method, this wont take so long...');
    else
    disp('Normalizing reduced method, this will be pretty swift...');
    end
    
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    
    colorLumi=colorSpace(:,:,1);
    grayLumi=graySpace(:,:,1);
    
    maxColorLumi=max(max(colorLumi));
    minColorLumi=min(min(colorLumi));
    
    maxGrayLumi=max(max(grayLumi));
    minGrayLumi=min(min(grayLumi));
    
    colorLumiDiff=maxColorLumi-minColorLumi;
    grayLumiDiff=maxGrayLumi-minGrayLumi;
    
    %Normalization using Max & Min differences   
    disp ('Normalizing...');    

        colorLumi=(colorLumi*255)/(255-colorLumiDiff);
        grayLumi=(grayLumi*255)/(255-grayLumiDiff);

    if methodNumber==4
            %Build a random colorTable using random coordinates
            disp ('Building random colortable...');   
            rng('shuffle'); %useful? I really don't know
            colorTable = zeros(12*colorX,1);
            lookupTableY = zeros(12*colorX,1);
            lookupTableX = zeros(12*colorX,1);
            
            for p=1:12*colorX
                j = randi([1 colorY],1,1);
                i = randi([1 colorX],1,1);
                colorTable(p) = colorLumi(i,j);

                lookupTableY(p) = j;
                lookupTableX(p) = i;
            end
    end

    %Compare all these new informations and assign to new image
    disp ('Comparing luminances & assigning to new image...');
    for i=1:grayX
        for j=1:grayY
                        
            %Search for closest number by subtracting the searched value by
            %all existing in the colorspace, the one(s) nearest to zero
            %will be the closests
            currentLumi=grayLumi(i,j);
            if methodNumber~=4
                temp=abs(colorLumi-currentLumi);
                minTemp=min(min(temp));
                [x,y] = find(temp==minTemp);
            else
                temp=abs(colorTable-currentLumi);
                minTemp=min(min(temp));
                index = find(temp==minTemp);
                x = lookupTableX(index);
                y = lookupTableY(index);
            end 
            
            if (minTemp > limitDifference && limitDifference > 0)
               continue 
            end
               
            if (isempty(x)~=1)    
                graySpace(i,j,2)=colorSpace(x(1),y(1),2);
                graySpace(i,j,3)=colorSpace(x(1),y(1),3);      
            end

       end
   end
 
else %Region method
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    if methodNumber == 1
    disp('Region low-contrast method chosen, will definitely take a while');
    else
    disp('Region high-contrast method chosen, this might take a while.....');    
    end
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    
    disp('Calculating means and deviations....');
    
    grayRemapDeviation = std(std(graySpace(:,:,1)));
    grayRemapMean = mean(mean(graySpace(:,:,1)));
    
    colorRemapDeviation = std(std(colorSpace(:,:,1)));
    colorRemapMean = mean(mean(colorSpace(:,:,1)));
    
    deviationDivision = grayRemapDeviation/colorRemapDeviation;
    
    %Luminance Remapping
    for i=1:colorX
        for j=1:colorY
        colorSpace(i,j,1) = deviationDivision*(colorSpace(i,j,1) - colorRemapMean ) + grayRemapMean;
        end
    end
    
    %Padding calculation, to use mean and standard deviation
    
    paddingAmount = 1; %only works with 1 (for now) //TODO
    paddingColor = padarray(colorSpace(:,:,1),[paddingAmount paddingAmount],0);
    paddingGray = padarray(graySpace(:,:,1),[paddingAmount paddingAmount],0);
    
    
    [paddingGraySizeX, paddingGraySizeY,~] = size(paddingGray);
    [paddingColorSizeX, paddingColorSizeY,~] = size(paddingColor);
    
    paddingGray(1,1) = graySpace (1,1);
    paddingGray(1,paddingGraySizeY) = graySpace (1,grayY);
    
    paddingGray(paddingGraySizeX,1) = graySpace (grayX,1);
    paddingGray(paddingGraySizeX,paddingGraySizeY) = graySpace (grayX,grayY);
    
    %Gray image padding
    for i = 2 : paddingGraySizeX-1
        paddingGray(i,1) = graySpace(i-1,1);
        paddingGray(i,paddingGraySizeY) = graySpace(i-1,grayY);
    end
    for i = 2 : paddingGraySizeY-1
        paddingGray(1,i) = graySpace(1,i-1);
        paddingGray(paddingGraySizeX,i) = graySpace(grayX,i-1);
    end
    
    %Color image padding
    for i = 2 : paddingColorSizeX-1
        paddingColor(i,1) = colorSpace(i-1,1);
        paddingColor(i,paddingColorSizeY) = colorSpace(i-1,colorY);
    end
    for i = 2 : paddingColorSizeY-1
        paddingColor(1,i) = colorSpace(1,i-1);
        paddingColor(paddingColorSizeX,i) = colorSpace(colorX,i-1);
    end
    
    
    %offset calculations to search for neighbors
    offsets = [-colorX-1-(paddingAmount*2) -1 colorX-1+(paddingAmount*2) ; 
                -colorX-(paddingAmount*2) 0 colorX+(paddingAmount*2); 
                -colorX+1-(paddingAmount*2) 1 colorX+1+(paddingAmount*2)];
            
    offsetsGray = [-grayX-1-(paddingAmount*2) -1 grayX-1+(paddingAmount*2) ; 
                -grayX-(paddingAmount*2) 0 grayX+(paddingAmount*2); 
                -grayX+1-(paddingAmount*2) 1 grayX+1+(paddingAmount*2)];
    
    if methodNumber == 1       
        disp('Calculating GrayTable....');

        %Initialization
        grayTableMean = zeros(grayX,grayY);
        grayTableDeviation = zeros(grayX,grayY);

        %Get the mean and stardard deviation of each pixel neighborhood's
        for j=1:grayY
            for i=1:grayX
                neighbors = paddingGray( (grayX + paddingAmount*2)*j+ i+paddingAmount +offsetsGray);
                grayTableMean(i,j) = mean(mean(neighbors));
                grayTableDeviation(i,j)= std(std(neighbors));
            end
        end
    end

    disp('Calculating ColorTable....');
    %Initialization
    colorTableMean = zeros(colorX,colorY);
    colorTableDeviation = zeros(colorX,colorY);
    colorCalc = zeros(colorX,colorY);
     
    %Get the mean and stardard deviation of each pixel neighborhood's
    for j=1:colorY
         for i=1:colorX
             neighbors = paddingColor( (colorX + paddingAmount*2)*j+ i+paddingAmount +offsets);
             colorTableMean(i,j) = mean(mean(neighbors));
             colorTableDeviation(i,j)= std(std(neighbors));
             colorCalc(i,j) = colorTableMean(i,j)/2 + colorTableDeviation(i,j)/2;
         end
     end
       
     disp('Searching for nearest luminance value....');

     for i=1:grayX
         for j=1:grayY
             
            %Search for closest number by subtracting the searched value by
            %all existing in the colorspace, the one(s) nearest to zero
            %will be the closests
            currentLumi = graySpace(i,j,1) ;
            temp = abs(colorSpace(:,:,1) - currentLumi);
            minTemp = min(min(temp));
            
            %Limit the difference in luminosity
            if (minTemp > limitDifference && limitDifference > 0)
               continue 
            end

            [x,y] = find(temp == minTemp);
            
            %Slice the amount of pixels to be considered a big amount
            %probably indicates same colors in different places
            if(length(x)>500)
                x = x(1:200);
                y = y(1:200);
            end
            
            %If we find nothing, just continue
            if (isempty(x)==1)
                continue
            end
            
            %Index different deviations to perform the search again
            deviations = zeros(length(x),1);
            
            for k=1:length(x)
                deviations(k) = colorTableDeviation(x(k),y(k));
            end
            
            %Search for the right amount of deviation
            
            %Low contrast
            if(methodNumber == 1) 
                
                %Initialization
                minimum = 42;
                if(length(deviations) > 1)
                    for yetAnotherIndexVariable=1 : length(deviations)
                        %Find the smallest difference between colorImage
                        %and grayImage standard deviation of luminosity
                        temp = abs(grayTableDeviation - deviations(yetAnotherIndexVariable) );
                        minTemp = min(min(temp));

                        if (minTemp > limitDifference && limitDifference > 0)
                            continue 
                        end

                        if yetAnotherIndexVariable~=1 || minimum > minTemp
                            minimum = minTemp;
                            [x,y] = find(temp == minTemp);
                        end

                    end
                else
                    [x,y] = find(colorTableDeviation == deviations(1));
                end
                
            %High contrast  
            else 
                deviationsLookup = min(deviations);
                [x,y] = find(colorTableDeviation == deviationsLookup);
            end
            
            if (isempty(x)~=1)
                graySpace(i,j,2) = colorSpace(x(1),y(1),2);
                graySpace(i,j,3) = colorSpace(x(1),y(1),3);
            end
            
         end
     end
     
end

    %Displays the image, finally
    subplot(1,2,1); %left
    imshow(grayImage);

    subplot(1,2,2); %right
    imshow(lab2rgb(graySpace));

    disp ('Done! Enjoy!');  

end